﻿using System.Collections.Generic;
using System.Linq;
using DetangledDigital.UHydrator;
using DetangledDigital.UHydrator.Attributes;
using DetangledDigital.UHydrator.Models;
using Umbraco.Core.Models;

namespace uHydratorExample.UI.Models
{
    public class HomeModel : BaseModel
    {
        private readonly ModelFactory _modelFactory = new ModelFactory();

        [UmbracoProperty("home_introduction_title", PropertyEditor.Textbox)]
        public string IntroductionTitle { get; set; }

        [UmbracoProperty("home_introduction_text", PropertyEditor.TinyMCE)]
        public string Introduction { get; set; }

        [UmbracoProperty("home_about_title", PropertyEditor.Textbox)]
        public string AboutTitle { get; set; }

        [UmbracoProperty("home_about_text", PropertyEditor.TinyMCE)]
        public string About { get; set; }

        [UmbracoProperty("home_services_title", PropertyEditor.Textbox)]
        public string ServicesTitle { get; set; }

        [UmbracoProperty("home_services_text", PropertyEditor.TinyMCE)]
        public string Services { get; set; }

        [UmbracoProperty("articles", PropertyEditor.MultiNodeTreePicker)]
        public IEnumerable<IPublishedContent> ArticleNodes { get; set; }

        public List<ArticleModel> ArticleModels
        {
            get
            {
                return ArticleNodes == null ? null : ArticleNodes.Select(articleNode => _modelFactory.Create<ArticleModel>(articleNode)).ToList();
            }
        }
    }
}
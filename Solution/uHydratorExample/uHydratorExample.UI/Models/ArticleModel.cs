﻿using System;
using DetangledDigital.UHydrator;
using DetangledDigital.UHydrator.Attributes;
using Our.Umbraco.PropertyConverters.Models;
using Umbraco.Core.Models;

namespace uHydratorExample.UI.Models
{
    public class ArticleModel : PagesModel
    {
        private readonly ModelFactory _modelFactory = new ModelFactory();

        [UmbracoProperty("bodyText", PropertyEditor.TinyMCEv3)]
        public string BodyText { get; set; }

        [UmbracoProperty("mainImage", PropertyEditor.MediaPicker)]
        public IPublishedContent MainImage { get; set; }

        [UmbracoProperty("time", PropertyEditor.DateTime)]
        public DateTime Time { get; set; }

        [UmbracoProperty("relatedLinks", PropertyEditor.RelatedLinks)]
        public RelatedLinks RelatedLinks { get; set; }

        [UmbracoProperty("relatedArticle", PropertyEditor.ContentPicker)]
        public IPublishedContent RelatedArticleNode { get; set; }

        public ArticleModel RelatedArticleModel
        {
            get
            {
                return RelatedArticleNode == null ? null : _modelFactory.Create<ArticleModel>(RelatedArticleNode);
            }
        }
    }
}
﻿using uHydratorExample.UI.Models;

namespace uHydratorExample.UI.Controllers
{
    using System.Web.Mvc;
    using Umbraco.Web.Models;
    using Umbraco.Web.Mvc;
    using uHydratorExample.UI.ViewModels;
    using DetangledDigital.UHydrator;

    public class ArticleController : RenderMvcController
    {
        private readonly ModelFactory _modelFactory;

        public ArticleController()
        {
            // Instantiate a factory.
            // If you use IoC you can wire this up to the IViewModelFactory interface and inject into the constructor.

            _modelFactory = new ModelFactory();
        }

        public override ActionResult Index(RenderModel model)
        {
            // Here's the magic, pass your own decorated view model (which must implement DetangledDigital.UHydrator.ViewModels.IBaseViewModel)
            // to the factory and it will populate those properties which have been decorated with the UmbracoProperty attribute.
            //
            // Dont forget to check the attributes on the ArticleModel.
            //
            // Having toruble? Tweet me - @CGaskell

            var viewModel = new ArticleViewModel()
            {
                ArticleModel = _modelFactory.Create<ArticleModel>(model.Content)
            };
            
            return CurrentTemplate(viewModel);
        }
    }
}
﻿namespace DetangledDigital.UHydrator
{
    using Services;
    using Models;
    using Umbraco.Core.Models;

    /// <summary>
    ///     @CGASKELL:  Custom factory that will attempt to hydrate models implementing
    ///     IBaseModel and with properties decorated with [UmbracoPropertyAttribute]
    /// </summary>
    public class ModelFactory : IModelFactory
    {
        private readonly IHydrator _hydrator;

        public ModelFactory()
        {
            _hydrator = new Hydrator();
        }

        public T Create<T>(IPublishedContent content) where T : IBaseModel, new()
        {
            var model = new T();
            return Set(model, content);
        }

        public T Set<T>(T model, IPublishedContent content) where T : IBaseModel
        {
            // Populate values marked from attributes
            _hydrator.HydrateModel(model, content);
            return model;
        }
    }
}
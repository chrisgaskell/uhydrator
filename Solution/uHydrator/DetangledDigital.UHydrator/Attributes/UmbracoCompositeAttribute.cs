﻿namespace DetangledDigital.UHydrator.Attributes
{
    using System;

    [AttributeUsage(AttributeTargets.Property)]
    public class UmbracoCompositeAttribute : Attribute
    {
       
    }
}
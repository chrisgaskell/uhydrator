Set-ExecutionPolicy RemoteSigned
Import-Module WebAdministration

# workaround for ISE executing script from c:\windows\system32
Push-Location $MyInvocation.MyCommand.Definition.Replace($MyInvocation.MyCommand.Name, "")

Function bootstrap($solutionName, $siteName, $hostHeader, $sitePath, $pipelineMode, $appPoolIdentity, $appPoolVersion)
{  
    $site = Get-WebSite | where { $_.Name -eq $siteName }
    if($site -eq $null)
    {
        echo "Creating site: $siteName"

		echo "Creating application pool"
        $appPool = New-WebAppPool -Name $siteName -Force
		$appPool.processModel.identityType = $appPoolIdentity				
		$appPool.managedPipelineMode = $pipelineMode
		$appPool.managedRuntimeVersion = $appPoolVersion
		$appPool | Set-Item		
	   
	    echo "Creating website"
        $absoluteSitePath = Resolve-Path $sitePath
        New-Website -Name $siteName -Port 80 -PhysicalPath $absoluteSitePath -ApplicationPool $siteName -HostHeader $hostHeader
        
		echo "Setting file/folder permissions for: $sitePath"			
		$inherit = [system.security.accesscontrol.InheritanceFlags]"ContainerInherit, ObjectInherit"
		$propagation = [system.security.accesscontrol.PropagationFlags]"None"
		$acl = Get-Acl $absoluteSitePath
		$accessrule = New-Object system.security.AccessControl.FileSystemAccessRule($appPoolIdentity, "FullControl", $inherit, $propagation, "Allow")
		$acl.AddAccessRule($accessrule)
		Set-Acl -aclobject $acl $absoluteSitePath 
		
		addHostFileEntry($hostHeader)
    }
	else
	{	
		echo "Site with the name $siteName already exists"
	}
}

Function addHostFileEntry($hostHeader)
{
	echo "Writing host file entry"
		"127.0.0.1" + "`t" + $hostHeader | Out-File -encoding ASCII -append "C:\Windows\System32\drivers\etc\hosts"
}

$siteName = "uHydratorExample"
bootstrap "$siteName.local" "$siteName" "$siteName.local" "Solution\uHydratorExample\uHydratorExample.UI" "Integrated" "NetworkService" v4.0

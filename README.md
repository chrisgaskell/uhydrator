# uHydrator

A light weight reflective mapper to populate view models based on the content within the Umbraco IPublished item. uHydrator will also populate composite objects (works well with mixins).

**See the example Umbraco site in /Solution/uHydratorExample/uHydratorExample.UI/ for a working demo.**

**Note, uHydrator is currently compiled again Umbraco v6.1.6. Version support is coming soon.**

Get in touch @CGaskell


## Usage
Some basic setup steps, also checkout the example full Umbraco install.

1. Hijack your umbraco route by adding a controller. See here for more info [http://our.umbraco.org/documentation/Reference/Mvc/custom-controllers](http://our.umbraco.org/documentation/Reference/Mvc/custom-controllers)


2. Create a view model for this route, say 'HomeViewModel'. Have this viewmodel inherit DetangledDigital.UHydrator.ViewModels.BaseViewModel (or you can implement DetangledDigital.UHydrator.ViewModels.IBaseViewModel yourself if you like). This adds some base properties:

	    public abstract class BaseViewModel : IBaseViewModel
	    {
		    public int Id { get; set; }
		    public string Name { get; set; }
		    public string Path { get; set; }
		    public string Url { get; set; }
	    }
	    

3. Lets say in the CMS you have two properties on your 'Home' document type, "Title" and "Introduction". You give these fields a different alias in the CMS to ensure they're unique. You could now create your HomeViewModel as:

	    using DetangledDigital.UHydrator.Attributes;
	    using DetangledDigital.UHydrator.ViewModels;
	    
	    public class HomeViewModel : BaseViewModel, IBaseViewModel
	    {
		    [UmbracoProperty("home_title", PropertyEditor.Textbox)]
		    public string IntroductionTitle { get; set; }
		    
		    [UmbracoProperty("home_introduction", PropertyEditor.TinyMCE)]
		    public string Introduction { get; set; }
	    }


4. Have your view use the derived class in your home view (in this example 'HomeViewModel'). I use the syntax below to keep Umbraco involved

		@inherits UmbracoViewPage<HomeViewModel>


5. The last step is to use the uHydrator factory in your controller action. You could do this as:

	    using System.Web.Mvc;
	    using Umbraco.Web.Models;
	    using Umbraco.Web.Mvc;
	
	    using uHydratorExample.UI.ViewModels;
	    using DetangledDigital.UHydrator;
	
	    public class HomeController : RenderMvcController
	    {
	        private readonly ViewModelFactory _viewModelFactory;
	
	        public HomeController()
	        {
	            // Instantiate a factory.
	            // If you use IoC you can wire this up to the IViewModelFactory interface and inject into the constructor.
	
	            _viewModelFactory = new ViewModelFactory();
	        }
	
	        public override ActionResult Index(RenderModel model)
	        {
	            var viewModel = _viewModelFactory.Create<HomeViewModel>(model.Content);
	            return CurrentTemplate(viewModel);
	        }
	    }


6. uHydrator will also handle composites. You can use the [UmbracoComposite] attribute above nested classes.

## Install the Umbraco uHydrator Example Site 
### Site Setup

* Checkout the develop branch
* Run Powershell as administrator. Run 'Set-ExecutionPolicy RemoteSigned'
* Run the install.ps1 powershell script using '.\install.ps1'. This will setup IIS, disk permissions and update the hosts file
* Navigate to [http://uhydratorexample.local/](http://uhydratorexample.local/)

_Note: If you run powershell scripts in windows 8 you'll need to set powershell to version 2 by opening powershell (run as admin) then executing 'powershell -version 2.0'_

### URLs

*  [http://uhydratorexample.local/](http://uhydratorexample.local/) - homepage.
*  [http://uhydratorexample.local/umbraco/](http://uhydratorexample.local/umbraco/) - CMS Backoffice.

### CMS User Account
* User: Admin           Pass: admin

